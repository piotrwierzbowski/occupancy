package pl.piotrwierzbowski.occupancy.service;

import java.util.List;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import pl.piotrwierzbowski.occupancy.model.CalculateProfitsResponse;
import pl.piotrwierzbowski.occupancy.model.RoomTypeProfitCalculation;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { OccupancyService.class })
public class OccupancyServiceTest {
    
    @Autowired
    private OccupancyService occupancyService; 
    
    @ParameterizedTest
    @MethodSource("provideTestData")
    public void testCalculateProfits(int freePremiumCount, int freeEconomicCount, List<Integer> guestsOffers, RoomTypeProfitCalculation premiumCalculation, RoomTypeProfitCalculation economicCalculation) {
        // when
        CalculateProfitsResponse result = occupancyService.calculateProfits(freePremiumCount, freeEconomicCount, guestsOffers);
        
        // then
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result.getEconomic()).isNotNull();
        Assertions.assertThat(result.getPremium()).isNotNull();
        Assertions.assertThat(result.getEconomic()).isEqualTo(economicCalculation);
        Assertions.assertThat(result.getPremium()).isEqualTo(premiumCalculation);
    }
    
    private static Stream<Arguments> provideTestData() {
        var guestsOffers = List.of(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
        
        return Stream.of(
          //task provided cases
          Arguments.of(3, 3, guestsOffers, 
                  new RoomTypeProfitCalculation(3, 738), new RoomTypeProfitCalculation(3, 167)),
          Arguments.of(7, 5, guestsOffers, 
                  new RoomTypeProfitCalculation(6, 1054), new RoomTypeProfitCalculation(4, 189)),
          Arguments.of(2, 7, guestsOffers, 
                  new RoomTypeProfitCalculation(2, 583), new RoomTypeProfitCalculation(4, 189)),
          Arguments.of(7, 1, guestsOffers, 
                  new RoomTypeProfitCalculation(7, 1153), new RoomTypeProfitCalculation(1, 45)),
          
          //edge-cases
          Arguments.of(0, 0, guestsOffers, 
                  new RoomTypeProfitCalculation(0, 0), new RoomTypeProfitCalculation(0, 0)),
          Arguments.of(2, 2, List.of(10, 10, 10, 10, 10, 10, 10, 10, 10, 10), 
                  new RoomTypeProfitCalculation(2, 20), new RoomTypeProfitCalculation(2, 20)),
          Arguments.of(2, 2, List.of(100, 100, 100, 100, 100, 100, 100, 100, 100, 100), 
                  new RoomTypeProfitCalculation(2, 200), new RoomTypeProfitCalculation(0, 0)),
          Arguments.of(10, 10, Lists.emptyList(), 
                  new RoomTypeProfitCalculation(0, 0), new RoomTypeProfitCalculation(0, 0))
        );
    }
}
