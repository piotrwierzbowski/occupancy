package pl.piotrwierzbowski.occupancy.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import pl.piotrwierzbowski.occupancy.model.CalculateProfitsResponse;
import pl.piotrwierzbowski.occupancy.model.RoomTypeProfitCalculation;

@Service
public class OccupancyService {
    
    private int premiumPriceLevel = 100;
    
    public CalculateProfitsResponse calculateProfits(int freePremiumCount, int freeEconomicCount, List<Integer> guestsOffers) {
        List<Integer> premiumRoomsBookings = new ArrayList<>();
        List<Integer> economicRoomsBookings = new ArrayList<>();
        
        List<Integer> orderedGuestsOffers = guestsOffers.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
        List<Integer> orderedPremiumGuestsOffers = orderedGuestsOffers.stream()
                .filter(offer -> offer >= premiumPriceLevel)
                .collect(Collectors.toList());
        List<Integer> orderedEconomicGuestsOffers = orderedGuestsOffers.stream()
                .filter(offer -> offer < premiumPriceLevel)
                .collect(Collectors.toList());

        premiumRoomsBookings = orderedPremiumGuestsOffers.stream()
            .limit(freePremiumCount)
            .collect(Collectors.toList());
        int premiumRoomsLeftForEconomicGuests = freePremiumCount - premiumRoomsBookings.size();
        
        if (freeEconomicCount < orderedEconomicGuestsOffers.size()) {
            orderedEconomicGuestsOffers.stream()
                    .limit(premiumRoomsLeftForEconomicGuests)
                    .forEach(premiumRoomsBookings::add);
            
            economicRoomsBookings = orderedEconomicGuestsOffers.stream()
                    .skip(premiumRoomsLeftForEconomicGuests)
                    .limit(freeEconomicCount)
                    .collect(Collectors.toList());
        } else {
            economicRoomsBookings = orderedEconomicGuestsOffers.stream()
                    .collect(Collectors.toList());
        }
        
        CalculateProfitsResponse result = CalculateProfitsResponse.builder()
                .premium(offersToRoomTypeProfitCalculation(premiumRoomsBookings))
                .economic(offersToRoomTypeProfitCalculation(economicRoomsBookings))
                .build();
        
        return result;
    }
    
    private RoomTypeProfitCalculation offersToRoomTypeProfitCalculation(List<Integer> offers) {
        return new RoomTypeProfitCalculation(offers.size(), offers.stream().mapToInt(Integer::intValue).sum());
    }
}
