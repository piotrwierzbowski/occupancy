package pl.piotrwierzbowski.occupancy.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoomTypeProfitCalculation {
    
    private int occupiedCount;
    
    private int calculatedProfit;
}
