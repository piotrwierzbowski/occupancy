package pl.piotrwierzbowski.occupancy.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CalculateProfitsResponse {
 
    private RoomTypeProfitCalculation premium;
    
    private RoomTypeProfitCalculation economic;
}
