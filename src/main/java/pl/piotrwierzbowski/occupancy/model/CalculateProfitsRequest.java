package pl.piotrwierzbowski.occupancy.model;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CalculateProfitsRequest {
    
    @NotNull
    private Integer freePremiumCount;
    
    @NotNull
    private Integer freeEconomicCount;

    @NotEmpty
    private List<Integer> guestsOffers;
}
