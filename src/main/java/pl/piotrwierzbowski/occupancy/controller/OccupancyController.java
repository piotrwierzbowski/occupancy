package pl.piotrwierzbowski.occupancy.controller;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import pl.piotrwierzbowski.occupancy.model.CalculateProfitsRequest;
import pl.piotrwierzbowski.occupancy.model.CalculateProfitsResponse;
import pl.piotrwierzbowski.occupancy.service.OccupancyService;

@RestController
@RequestMapping(value = "/occupancy", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class OccupancyController {

    private final OccupancyService occupancyService;
    
    @PostMapping()
    public CalculateProfitsResponse calculateProfits(@Valid @RequestBody CalculateProfitsRequest calculateProfitsRequest) {
        return occupancyService.calculateProfits(calculateProfitsRequest.getFreeEconomicCount(), calculateProfitsRequest.getFreePremiumCount(), calculateProfitsRequest.getGuestsOffers());
    }
    
}