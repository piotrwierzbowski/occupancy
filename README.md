# occupancy application - based on CodingChallengeBE

## System requirements
* Java 11
* Your favorite IDE if you want to edit tests
* Web browser or some REST service client to execute the program 

## Tests
To run the tests execute:
> ./gradlew test 

To change test cases go to /occupancy/src/test/java/pl/piotrwierzbowski/occupancy/service/OccupancyServiceTest.java and edit cases in provideTestData() operation. 
The cases needs to be provided in Junit5 parametrized tests format

## Running the program
To startup the service run the following command:
> ./gradlew bootRun

After a while the service should be deployed and ready for service, try it out
* via browser and swagger UI: http://localhost:8080/swagger-ui/#/occupancy-controller/calculateProfitsUsingPOST
* via REST interface: http://localhost:8080/occupancy

Call example:
> curl -X POST "http://localhost:8080/occupancy" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"freeEconomicCount\": 2, \"freePremiumCount\": 2, \"guestsOffers\": [ 100, 200, 300 ]}"

API docs:
> http://localhost:8080/v2/api-docs